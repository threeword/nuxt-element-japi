module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended',
  ],
  // 校验 .vue 文件
  plugins: [
    'vue'
  ],
  // 自定义规则
  rules: {
    /* 允许console */
    'no-console': [0],
    /* 标签属性数量 */
    'vue/max-attributes-per-line': [2, {
      singleline: 6,
      multiline: {
        max: 10,
        allowFirstLine: false
      }
    }],
    /* 要求 return 语句要么总是指定返回的值，要么不指定 */
    'consistent-return': [0],
    /* 强制使用一致的缩进 */
    indent: [1, 2],
    /* 禁止 if 语句中 return 语句之后有 else 块 */
    'no-else-return': [1],
    /* 禁止或强制使用分号代替 ASI */
    semi: [1, 'always'],
    /* 禁止使用多个空格 */
    'no-multi-spaces': [1],
    /* 要求操作符周围有空格 */
    'space-infix-ops': [2],
    /* 禁止或强制在括号内使用空格 */
    'array-bracket-spacing': [2, 'never'],
    /* 禁止或强制在代码块中开括号前和闭括号后有空格 */
    'block-spacing': [2, 'always'],
    /* 禁止或强制在计算属性中使用空格 */
    'computed-property-spacing': [2, 'never'],
    /* 禁止或强制在函数标识符和其调用之间有空格 */
    'func-call-spacing': [2, 'never'],
    /* 强制关键字周围空格的一致性 */
    'keyword-spacing': [2, { before: true }],
    /* 强制在逗号周围使用空格 */
    'comma-spacing': [2, { before: false, after: true }],
    /* 强制在花括号中使用一致的空格 */
    'object-curly-spacing': [2, 'always'],
    /* 强制分号前后有空格 */
    'semi-spacing': ['error', { before: false, after: true }],
    /* 要求或禁止语句块之前的空格 */
    'space-before-blocks': [2],
    /* 要求或禁止函数圆括号之前有一个空格 */
    'space-before-function-paren': [2, 'never'],
    /* 禁止或强制圆括号内的空格 */ 
    'space-in-parens': [2, 'never'],
    /* 要求或禁止在注释前有空白 (space 或 tab) */
    'spaced-comment': ['error', 'always'],
    /* 强制在 switch 的冒号左右有空格 */
    'switch-colon-spacing': ['error', { before: false, after: true }],
    /* 要求箭头函数的箭头之前或之后有空格 */
    'arrow-spacing': 'error',
    /* 强制剩余和扩展运算符及其表达式之间有空格 */
    'rest-spread-spacing': ['error', 'never'],
    /* 要求遵循大括号约定 */
    curly: 'error',
    /* 大括号风格要求 */
    'brace-style': ['error', '1tbs', { allowSingleLine: false }],
    /* 要求对象字面量属性名称使用引号 */
    'quote-props': ['error', 'as-needed'],
    /* 强制使用一致的反勾号、双引号或单引号 */
    quotes: ['error', 'single'],
    /* 强制在对象字面量的键和值之间使用一致的空格 */
    'key-spacing': ['error', { afterColon: true, mode: 'strict' }],
    /* 转单标签 */
    'vue/html-self-closing': [0],
  }
};