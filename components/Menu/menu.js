const initMenu = [{
  title: '首页',
  path: '/',
  icon: 'iconfont icon-home',
  children: []
}, {
  title: '店铺',
  icon: 'iconfont icon-store',
  children: [{
    icon: 'iconfont icon-store',
    title: '店铺管理',
    path: '/store/store/'
  }, {
    icon: 'iconfont icon-product',
    title: '商品管理',
    path: '/store/product/'
  }, {
    icon: 'iconfont icon-order',
    title: '订单管理',
    path: '/store/order/'
  }, {
    icon: 'iconfont icon-yue',
    title: '预约管理',
    path: '/store/yue/'
  }]
}, {
  title: '用户',
  icon: 'iconfont icon-user',
  children: [{
    icon: 'iconfont icon-vip',
    title: '会员管理',
    path: '/user/vip/'
  }, {
    icon: 'iconfont icon-user',
    title: '普通用户',
    path: '/user/user/'
  }]
}];

const Obj = {
  flatMenu: [],
  menuKV: {}
};

const initData = (menu) => {
  Obj.menuKV = (function dos(children, result) {
    for (const m of children) {
      if (m.path) {
        result[m.title] = m.path; 
      }
      if (m.group && m.group.length > 0) {
        dos(m.group, result); 
      }
      if (m.children && m.children.length > 0) {
        dos(m.children, result); 
      }
    }
    return result;
  })(menu, {});

  console.log('菜单映射', Obj);
};

initData(initMenu);

export {
  initMenu,
  initData,
  Obj
};


