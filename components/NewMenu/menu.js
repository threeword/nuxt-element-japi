export const initMenu = [{
  title: '首页',
  path: '/',
  icon: 'iconfont icon-home',
  children: []
}, {
  title: '店铺',
  icon: 'iconfont icon-store',
  children: [{
    icon: 'iconfont icon-store',
    title: '个人装修',
    children: [{
      icon: 'iconfont icon-product',
      title: '店铺页面',
      path: '/store/page/'
    }, {
      icon: 'iconfont icon-order',
      title: '店铺导航',
      path: '/store/main-nav/'
    }]
  }, {
    icon: 'iconfont icon-store',
    title: '系统页面',
    children: [{
      icon: 'iconfont icon-product',
      title: '用户中心',
      path: '/store/user-center/'
    }]
  }]
}, {
  title: '商品',
  icon: 'iconfont icon-store',
  children: [{
    icon: 'iconfont icon-store',
    title: '商品管理',
    children: [{
      icon: 'iconfont icon-product',
      title: '全部商品',
      regPath: '/product/product/',
      path: '/product/product/list/'
    }, {
      icon: 'iconfont icon-order',
      title: '运费模板',
      path: '/product/freight/'
    }]
  }]
}, {
  title: '订单',
  icon: 'iconfont icon-store',
  children: [{
    icon: 'iconfont icon-store',
    title: '订单查询',
    children: [{
      icon: 'iconfont icon-product',
      title: '全部订单',
      path: '/order/list/'
    }]
  }, {
    icon: 'iconfont icon-store',
    title: '订单处理',
    children: [{
      icon: 'iconfont icon-product',
      title: '退款售后',
      path: '/order/refund/'
    }, {
      icon: 'iconfont icon-order',
      title: '评价管理',
      path: '/order/comment/'
    }]
  }]
}, {
  title: '设置',
  icon: 'iconfont icon-user',
  children: [{
    icon: 'iconfont icon-vip',
    title: '店铺信息',
    children: [{
      icon: 'iconfont icon-vip',
      title: '商户信息',
      path: '/setting/shop/',
    }, {
      icon: 'iconfont icon-vip',
      title: '商户认证',
      path: '/setting/progress/',
    }]
  }, {
    icon: 'iconfont icon-user',
    title: '店铺管理',
    children: [{
      icon: 'iconfont icon-user',
      title: '退货管理',
      path: '/setting/refund-setting/',
    }, {
      icon: 'iconfont icon-user',
      title: '通用设置',
      path: '/setting/general/',
    }]
  }]
}];