# japi

#### 介绍

nuxt + element + mobx + japi + eslint Vue后台管理框架

#### 前言

> 取其精华去其糟粕

#### 使用说明

### 配置api协议 详情可参考 [JAPI](https://gitee.com/threeword/japi)

``` js
/** apiConfig.js */

exports.apis = [
  {
    /** 接口命名（注意大写开头，会作为Class的名称） */
    name: 'ServicePublic',
    /** 配置 */
    opts: {
      /** api协议地址 */
      uri: '',
      /** 协议类型 (swagger_1 | swagger_2 | openapi_3 | api_blueprint | io_docs | google | raml | wadl) */
      spec: 'swagger_2',
      /** 需要生成文件类型 支持 js | ts (注意：Taro项目自带ts天赋，所以只支持ts) */
      type: 'ts',
      /** api接口服务地址 也可在config.js中 配置开发环境、测试环境、生成环境 */
      serverUrl: ''
    }
  },
  /** 自行补充其他协议配置 结构如上 */
];
```

### 更新api代码
```bash

npm run api

```

## 演示

![JUI](http://nuxt-element-japi.oss-cn-hangzhou.aliyuncs.com/1.png)
![JUI](http://nuxt-element-japi.oss-cn-hangzhou.aliyuncs.com/2.png)
![JUI](http://nuxt-element-japi.oss-cn-hangzhou.aliyuncs.com/3.png)
![JUI](http://nuxt-element-japi.oss-cn-hangzhou.aliyuncs.com/4.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
