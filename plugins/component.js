import Vue from 'vue';

/* 封装组件 */
import * as Components from '@/components';
Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name]);
});

/* 无标签组件 */
import { Plugin } from 'vue-fragment';
Vue.use(Plugin);

/* 富文本组件 */
import VueQuillEditor from 'vue-quill-editor';
Vue.use(VueQuillEditor);

/* element组件 */
import Element from 'element-ui';
import zhCN from 'element-ui/lib/locale/lang/zh-CN';
Vue.use(Element, { zhCN, size: 'small' });
