import Vue from 'vue';
import moment from 'moment';

export default function() {
  /* 金额 */
  Vue.filter('price', v => isNaN(Number(v)) ? '0.00' : Number(v).toFixed(2));
  /* 时间 */
  Vue.filter('time', (date, format = 'YYYY-MM-DD HH:mm') =>
    date ? moment(+moment.utc(date)).format(format) : ''
  );
}
