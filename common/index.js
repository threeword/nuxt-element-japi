import Cookies from 'js-cookie';
import { Message, MessageBox } from 'element-ui';
import moment from 'moment';
import { SERVICE_URL } from '@/constants';
import debugFactory from 'debug';
import { frameState } from '@/mobx/index';

const debug = debugFactory('worker:common');

export const errorDebug = debugFactory('worker:error-log');

export const env_target = process.env.target;

/** 确认弹框 */
export const showConfirm = (opts = { title: '提示', message: '是否删除！' }) => {
  const { title, message } = opts;
  return new Promise((resolve, reject) => {
    MessageBox.confirm(message, title, {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(resolve).catch(reject);
  });
};

export const clearSelectType = (search, selectType) => {
  selectType.forEach(type => Reflect.deleteProperty(search, type.key));
};

/** 持久化方法 */
export const persistence = (key, state, whiteList = []) => {
  /* 持久化白名单 */
  const persistenceData = {};
  whiteList.forEach(key => {
    persistenceData[key] = state[key];
  });
  
  setStore(key, persistenceData);
};

/**
 * 获取缓存数据
 * @param {*} key 
 * @param {*} isJson 是否是json 默认是
 */
export const getStore = (key, isJson = true) => {
  if (isJson) {
    ensure(() => {
      return JSON.parse(Cookies.get(key));
    }, null);
  } else {
    ensure(() => {
      return Cookies.get(key);
    }, null);
  }
};

export const setStore = (key, data) => {
  if (data instanceof Object) {
    Cookies.set(key, JSON.stringify(data));
  } else {
    Cookies.set(key, data);
  }
};

export const removeStore = key => {
  Cookies.remove(key);
};

export const getToken = () => {
  return Cookies.get('token');
};

export const setToken = (token) => {
  return Cookies.set('token', token);
};

/* 获取accessToken */
export const accessToken = () => {
  return { Authorization: 'Bearer ' + getToken() };
};


const showMsgFunc = (message, type) => {
  Message.closeAll();
  Message({
    message,
    type
  }); 
};

/* 提示错误 */
export const message = {
  warning: msg => showMsgFunc(msg, 'warning'),
  success: msg => showMsgFunc(msg, 'success'),
  info: msg => showMsgFunc(msg, 'info'),
  error: msg => showMsgFunc(msg, 'error'),
};

/* 转换图片地址 */
export const convertImgUrl = (fileToken, type = false) => {
  if (/http/.test(fileToken)) {
    return fileToken; 
  }

  let typeString = '';

  if (type == true) {
    typeString = '/raw';
  } else if (type == false) {
    typeString = '/image/128x128';
  } else {
    typeString = `/image/${type}`;
  }

  return fileToken && SERVICE_URL.serviceUpload + fileToken + typeString;
};

/* 刷新Dom 谨慎使用! */
export const refreshDom = (vue, attr) => {
  vue[attr] = false;
  vue.$nextTick(() => {
    vue[attr] = true;
  });
};

/* 格式化时间 */
export const dateFormat = (date, format = 'YYYY-MM-DD HH:mm') => {
  return moment(+moment.utc(date)).format(format);
};

export const formateTime = (time, is24) => {
  const second = String(~~(time / 1000) % 60);
  const min = String(~~(time / 1000 / 60) % 60);
  const hour = String(~~(time / 1000 / 60 / 60));
  if (is24) {
    return `${hour.length > 1 ? hour : 0 + hour}:${
      min.length > 1 ? min : 0 + min
    }`;
  }
  return `${hour.length > 1 ? hour : 0 + hour}:${
    min.length > 1 ? min : 0 + min
  }:${second.length > 1 ? second : 0 + second}`;
};

export const formatTimeDetail = (time) => {
  const second = String(~~(time / 1000) % 60).padStart(2, '0');
  const min = String(~~(time / 1000 / 60) % 60).padStart(2, '0');
  const hour = String(~~(time / 1000 / 60 / 60)).padStart(2, '0');
  return {
    hour,
    min,
    second
  };
};

export const calcLastTime = ({ start, end }) => {
  const is24 = (end - start) / 1000 / 60 / 60 >= 24;
  const endTime = end;
  const lastTime = formateTime(end - +new Date(), is24);
  return {
    is24,
    endTime,
    lastTime
  };
};


export const calcLastTime2 = (endTime, type) => {
  const seconds = +moment.utc(endTime) - Date.now();
  if (seconds <= 0) {
    return 0;
  }
  let day = ~~(seconds / 1000 / 60 / 60 / 24);
  let hours = ~~(seconds / 1000 / 60 / 60);
  const mines = ~~(seconds / 1000 / 60 % 60);
  if (type == 'day') {
    hours = ~~(seconds / 1000 / 60 / 60 % 24);
    return `${day}天${hours}小时${mines}分`;
  }
  return `${hours}小时${mines}分`;
};


export const fromValidate = (vue, ref = 'form') =>
  new Promise((resolve, reject) => {
    vue.$refs[ref].validate((valid) => {
      if (valid) {
        resolve();
      } else {
        reject(new Error('表单校验失败'));
      }
    });
  });

export const factoryRequired = (attrs) => {
  const obj = {};
  attrs.forEach(
    name =>
      (obj[name] = [
        {
          required: true,
          message: '不能为空'
        }
      ])
  );
  return obj;
};

export const factoryNumber = (attrs) => {
  const obj = {};
  attrs.forEach(
    name =>
      (obj[name] = [
        {
          message: '不能为空',
          type: 'number'
        }
      ])
  );
  return obj;
};

export const factoryRadio = (attrs, value = 0) => {
  const obj = {};
  attrs.forEach(name => (obj[name] = value));
  return obj;
};

export const formatRangeTime = times => {
  const newRange = {};
  Object.keys(times).forEach(key => {
    if (!times[key]) {
      return; 
    }
    if (/(Start|start)/.test(key)) {
      if (/:/.test(times[key])) {
        newRange[key] = times[key];
      } else {
        newRange[key] = times[key] + ' 00:00:00';
      }
    }
    if (/(End|end)/.test(key)) {
      if (/:/.test(times[key])) {
        newRange[key] = times[key];
      } else {
        newRange[key] = times[key] + ' 23:59:59';
      }
    }
  });
  return newRange;
};

export const ensure = (func, defaults) => {
  try {
    return func();
  } catch (e) {
    debug(e);
    return defaults;
  }
};

/** 路由改变事件
 * 
 * @param {*} menu: 菜单  path:
 */
export const onRouterChange = ({ menu, path }) => {
  const menuList = menu || frameState.state.menuList;

  const _path = path || window.location.pathname;
  let childIndex = '0_0';
  let parentIndex = -1;
  let breadcrumbData = [];

  if (_path !== '/') {
    parentIndex = menuList.findIndex(menu => {
      if (menu.path == '/' || !menu.path) {
        return false;
      }
      const reg = new RegExp(menu.path);
      return reg.test(_path);
    });
    if (parentIndex == -1) {
      for (let p = 0; p < menuList.length; p++) {
        const parentMenu = menuList[p];
        for (let i = 0; i < parentMenu.children.length; i++) {
          const childMenu = parentMenu.children[i];
          if (childMenu.children) {
            for (let j = 0; j < childMenu.children.length; j++) {
              const cChildMenu = childMenu.children[j];
              const reg = new RegExp(cChildMenu.regPath || cChildMenu.path);
              if (reg.test(_path)) {
                childIndex = `${i}_${j}`;
                parentIndex = p;
                breadcrumbData = [childMenu, cChildMenu];
                break;
              }
            }
          }
        }
      }
    }
  } else {
    parentIndex = 0;
  }

  frameState.setFrameState({
    parentMenuIndex: parentIndex,
    childMenuIndex: childIndex,
    breadcrumbData,
  });

};