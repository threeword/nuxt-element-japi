const config = require('../apis/config');

/** 获取店铺id */
export const getStoreId = () => {
  return 142;
};

/** 接口服务地址 */
export const SERVICE_URL = {
  ...config.serverConfig,
};

/** 环境变量 */
export const projectEnv = config.env;

/** 页面路由 */
export const PAGE_URL = {
  home: '/',
  login: '/passport/login/'
};

/** 初始页码 */
export const pagination = {
  pageIndex: 1,
  pageSize: 10
};

/** 手机区号 */
export const phoneCode = [{ key: '0086', value: '+86', name: '中国大陆' }, { key: '001', value: '+01', name: '美国' }];

/** 商品上下架状态 */
export const shelfStatusMap = {
  0: '上架',
  1: '下架',
};

/** 贸易类型 */
export const tradeTypeMap = {
  1: '普通贸易',
  2: '保税备货',
  3: '海外直邮'
};

/** 支付平台 */
export const payPlatformMap = {
  0: '支付宝',
  1: '微信支付',
  2: '银联支付'
};

/** 订单来源 */
export const orderSourceMap = {
  0: 'PC商城',
  1: '微信商城'
};

/** 订单状态 */
export const orderStatusMap = {
  '-10': '订单作废',
  0: '订单取消',
  10: '未付款',
  20: '已付款',
  30: '等待申报支付单',
  40: '等待申报订单',
  50: '等待海关清关放行',
  60: '已发货',
  70: '已签收',
  80: '交易成功',
  90: '交易关闭'
};


export const colorMap = { 
  1: '#0091FF',
  2: '#E23D38',
  3: '#F47503',
};