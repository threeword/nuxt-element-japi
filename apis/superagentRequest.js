import request from "superagent";
import {onBeforeRequest, onAfterRequest, onErrorRequest} from './hooks';

function serializeQueryParams(parameters) {
  const str = [];
  for(const p in parameters) {
    if(parameters.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(parameters[p]));
    }
  }
  return str.join("&");
}

export const superagentRequest = async (
  _method,
  _url,
  _body,
  _headers,
  _queryParameters,
  form = {},
  reject,
  resolve
) => {

  let {
    method,
    url,
    body,
    headers,
    queryParameters,
  } = onBeforeRequest({
    method: _method,
    url: _url,
    body: _body,
    headers: _headers,
    queryParameters: _queryParameters,
  }, reject, resolve);

  const queryParams =
    queryParameters && Object.keys(queryParameters).length
      ? serializeQueryParams(queryParameters)
      : null;

  const urlWithParams = url + (queryParams ? "?" + queryParams : "");

  if(body && !Object.keys(body).length) {
    body = undefined;
  }

  let req = request(method, url).query(queryParameters);

  Object.keys(headers).forEach(key => {
    req.set(key, headers[key]);
  });

  if(body) {
    req.send(body);
  }

  if(typeof (body) === 'object' && !(body.constructor.name === 'Buffer')) {
    req.set('Content-Type', 'application/json');
  }

  if(Object.keys(form).length > 0) {
    req.type('form');
    req.send(form);
  }

  req.end((error, response) => {
    if(error || !response.ok) {
      onErrorRequest(response, resolve, reject);
    } else {
      onAfterRequest(response, resolve, reject)
    }
  });
};
