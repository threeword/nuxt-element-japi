import { serverConfig } from './config';

import ServicePublic from './ServicePublic'; 
export const servicePublic = new ServicePublic(serverConfig.servicePublic);

import ServiceStore from './ServiceStore'; 
export const serviceStore = new ServiceStore(serverConfig.serviceStore);

import ServiceUpload from './ServiceUpload'; 
export const serviceUpload = new ServiceUpload(serverConfig.serviceUpload);