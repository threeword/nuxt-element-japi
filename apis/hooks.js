import Cookies from 'js-cookie';
import {Message} from 'element-ui';
import {PAGE_URL} from '@/constants/index';

const setToken = (token) => {
  return Cookies.set('token', token);
};

const showMsgFunc = (message, type) => {
  Message.closeAll();
  Message({
    message,
    type
  });
};

/* 提示错误 */
const showMessage = {
  warning: msg => showMsgFunc(msg, 'warning'),
  success: msg => showMsgFunc(msg, 'success'),
  info: msg => showMsgFunc(msg, 'info'),
  error: msg => showMsgFunc(msg, 'error'),
};

/* TODO：实现授权 */
const getToken = () => {
  const token = Cookies.get('token');
  return token ? 'Bearer ' + token : token;
}

/** HTTP 状态码 */
const STATUS_CODE = {
  SUCCESS: 200,
  NO_ACCESS: 401
}

/**
 * 请求前
 * @param req 请求信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onBeforeRequest = (req, resolve, reject) => {
  const {method, url, body, headers, queryParameters} = req;
  return {...req, headers: {Authorization: getToken()}};
}

/**
 * 请求返回
 * @param res 返回信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onAfterRequest = (res, resolve, reject) => {
  const {statusCode, header, data: resData, body} = res;

  if(header.new_access_token) {
    console.info('token更新', header);
    setToken(header.new_access_token)
  }

  const {data, message, status} = resData || body;

  if(status == 1) {
    resolve(data);
  } else {
    showMessage.error(message)
    reject(message);
  }
}

/**
 * 请求失败
 * @param res 返回信息
 * @param resolve Promise.resolve
 * @param reject Promise.reject
 */
export const onErrorRequest = (res, resolve, reject) => {
  const {statusCode, header, data, body} = res;

  if(statusCode == STATUS_CODE.NO_ACCESS) {
    showMessage.error('登录已过期，请重新登录')
    /* 防止套娃 */
    if(/\/login\//.test(location.pathname)) {
      return;
    } else {
      const redirectUrl = PAGE_URL.login + '?back=' + encodeURIComponent(location.pathname)
      window.location.href = redirectUrl;
    }
    reject('登录已过期，请重新登录');
  } else {
    showMessage.error('请求失败')
    reject('请求失败');
  }
}
