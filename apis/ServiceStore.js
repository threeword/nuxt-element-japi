/*jshint esversion: 6 */
/*global fetch, btoa */
import Q from 'q';
import {superagentRequest} from "./superagentRequest";
/**
 * 
 * @class ServiceStore
 * @param {(string|object)} [domainOrOptions] - The project domain or options object. If object, see the object's optional properties.
 * @param {string} [domainOrOptions.domain] - The project domain
 * @param {object} [domainOrOptions.token] - auth token - object with value property and optional headerOrQueryName and isQuery properties
 */
let ServiceStore = (function() {
    'use strict';

    function ServiceStore(options) {
        let domain = (typeof options === 'object') ? options.domain : options;
        this.domain = domain ? domain : '';
        if (this.domain.length === 0) {
            throw new Error('Domain parameter must be specified as a string.');
        }
        this.apiKey = (typeof options === 'object') ? (options.apiKey ? options.apiKey : {}) : {};
    }

    function serializeQueryParams(parameters) {
        let str = [];
        for (let p in parameters) {
            if (parameters.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent(parameters[p]));
            }
        }
        return str.join('&');
    }

    function mergeQueryParams(parameters, queryParameters) {
        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    let parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }
        return queryParameters;
    }

    /**
     * HTTP Request
     * @method
     * @name ServiceStore#request
     * @param {string} method - http method
     * @param {string} url - url to do request
     * @param {object} parameters
     * @param {object} body - body parameters / object
     * @param {object} headers - header parameters
     * @param {object} queryParameters - querystring parameters
     * @param {object} form - form data object
     * @param {object} deferred - promise object
     */
    ServiceStore.prototype.request = function(method, url, parameters, body, headers, queryParameters, form, deferred) {
        const queryParams = queryParameters && Object.keys(queryParameters).length ? serializeQueryParams(queryParameters) : null;
        const urlWithParams = url + (queryParams ? '?' + queryParams : '');

        if (body && !Object.keys(body).length) {
            body = undefined;
        }

        fetch(urlWithParams, {
            method,
            headers,
            body: JSON.stringify(body)
        }).then((response) => {
            return response.json();
        }).then((body) => {
            deferred.resolve(body);
        }).catch((error) => {
            deferred.reject(error);
        });
    };

    /**
     * Set Api Key
     * @method
     * @name ServiceStore#setApiKey
     * @param {string} value - apiKey's value
     * @param {string} headerOrQueryName - the header or query name to send the apiKey at
     * @param {boolean} isQuery - true if send the apiKey as query param, otherwise, send as header param
     */
    ServiceStore.prototype.setApiKey = function(value, headerOrQueryName, isQuery) {
        this.apiKey.value = value;
        this.apiKey.headerOrQueryName = headerOrQueryName;
        this.apiKey.isQuery = isQuery;
    };
    /**
     * Set Auth headers
     * @method
     * @name ServiceStore#setAuthHeaders
     * @param {object} headerParams - headers object
     */
    ServiceStore.prototype.setAuthHeaders = function(headerParams) {
        let headers = headerParams ? headerParams : {};
        if (!this.apiKey.isQuery && this.apiKey.headerOrQueryName) {
            headers[this.apiKey.headerOrQueryName] = this.apiKey.value;
        }
        return headers;
    };

    /**
     * 登录授权[前端请求该接口]
     * @method
     * @name ServiceStore#Login
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.Login = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Account/Login';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 用户登录授权【授权中心专用】
     * @method
     * @name ServiceStore#GetUserLogin
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.GetUserLogin = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Account/GetUserLogin';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加订单信息
     * @method
     * @name ServiceStore#SaoMaPayNotify
     * @param {object} parameters - method options and parameters
     */
    ServiceStore.prototype.SaoMaPayNotify = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/AliPayNotify/SaoMaPayNotify';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 退保
     * @method
     * @name ServiceStore#QuitBondPay
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.QuitBondPay = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/QuitBondPay';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 是否缴纳成功
     * @method
     * @name ServiceStore#CheckPaySuccess
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.bondNumber - 保证金编号
     */
    ServiceStore.prototype.CheckPaySuccess = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/CheckPaySuccess';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['bondNumber'] !== undefined) {
            queryParameters['bondNumber'] = parameters['bondNumber'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取保证金凭证信息
     * @method
     * @name ServiceStore#GetBondPayVoucher
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.bondNumber - 保证金编号
     */
    ServiceStore.prototype.GetBondPayVoucher = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/GetBondPayVoucher';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['bondNumber'] !== undefined) {
            queryParameters['bondNumber'] = parameters['bondNumber'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 立即缴纳
     * @method
     * @name ServiceStore#Payment
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 保证金订单实体
     */
    ServiceStore.prototype.Payment = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/Payment';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取缴纳保证金记录--分页
     * @method
     * @name ServiceStore#GetBondPayPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {integer} parameters.userType - 用户类型（0=个人、1=店铺）
     * @param {string} parameters.startPayTime - 缴纳开始时间
     * @param {string} parameters.endPayTime - 缴纳结束时间
     * @param {integer} parameters.bondType - 缴纳保证金类型
     * @param {integer} parameters.status - 保证金状态
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetBondPayPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/GetBondPayPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        /** set default value **/
        queryParameters['userType'] = 1;

        if (parameters['userType'] !== undefined) {
            queryParameters['userType'] = parameters['userType'];
        }

        if (parameters['startPayTime'] !== undefined) {
            queryParameters['startPayTime'] = parameters['startPayTime'];
        }

        if (parameters['endPayTime'] !== undefined) {
            queryParameters['endPayTime'] = parameters['endPayTime'];
        }

        /** set default value **/
        queryParameters['bondType'] = -1;

        if (parameters['bondType'] !== undefined) {
            queryParameters['bondType'] = parameters['bondType'];
        }

        /** set default value **/
        queryParameters['status'] = -1;

        if (parameters['status'] !== undefined) {
            queryParameters['status'] = parameters['status'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取保证金流水记录--分页
     * @method
     * @name ServiceStore#GetBondFlowingWaterPagedList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.bondNumber - 保证金编号
     * @param {string} parameters.startCreateTime - 创建开始时间
     * @param {string} parameters.endCreateTime - 创建结束时间
     * @param {integer} parameters.type - 类型：收入，支出
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetBondFlowingWaterPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/GetBondFlowingWaterPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['bondNumber'] !== undefined) {
            queryParameters['bondNumber'] = parameters['bondNumber'];
        }

        if (parameters['startCreateTime'] !== undefined) {
            queryParameters['startCreateTime'] = parameters['startCreateTime'];
        }

        if (parameters['endCreateTime'] !== undefined) {
            queryParameters['endCreateTime'] = parameters['endCreateTime'];
        }

        if (parameters['type'] !== undefined) {
            queryParameters['type'] = parameters['type'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取保证金概览
     * @method
     * @name ServiceStore#GetBondPayStatistic
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetBondPayStatistic = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/GetBondPayStatistic';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取保证金缴纳信息
     * @method
     * @name ServiceStore#GetBondPayByBondNumber
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.bondNumber - 保证金编号
     */
    ServiceStore.prototype.GetBondPayByBondNumber = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Bond/GetBondPayByBondNumber';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['bondNumber'] !== undefined) {
            queryParameters['bondNumber'] = parameters['bondNumber'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取后台类目，通过父级获取
     * @method
     * @name ServiceStore#GetBackstageCategory
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.parentId - 父级类目id
     * @param {string} parameters.keys - 更新关键词搜索
     */
    ServiceStore.prototype.GetBackstageCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Category/GetBackstageCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['parentId'] !== undefined) {
            queryParameters['parentId'] = parameters['parentId'];
        }

        if (parameters['keys'] !== undefined) {
            queryParameters['keys'] = parameters['keys'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 通过后台类目id获取后台类目绑定规格，属性包含属性值
     * @method
     * @name ServiceStore#GetCategoryAttrByCategoryID
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.categoryId - 
     */
    ServiceStore.prototype.GetCategoryAttrByCategoryID = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Category/GetCategoryAttrByCategoryID';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['categoryId'] !== undefined) {
            queryParameters['categoryId'] = parameters['categoryId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据搜索Key或者isChina，查询原产地
    以下1、2两点条件互斥；
    1、Key,查询原产地名称，原产地拼音；
    2、isChina，查询原产地为中国，数据有且只有一条。
    3、Code,查询指定编码的原产地
     * @method
     * @name ServiceStore#GetCountryOfOrigin
     * @param {object} parameters - method options and parameters
         * @param {string} parameters.code - 原产地code,空字符串查询所有
         * @param {string} parameters.key - 搜索关键字
         * @param {integer} parameters.isChina - -1查询所有，0=默认中国，1=国外
     */
    ServiceStore.prototype.GetCountryOfOrigin = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/CountryOfOrigin/GetCountryOfOrigin';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['code'] !== undefined) {
            queryParameters['code'] = parameters['code'];
        }

        if (parameters['key'] !== undefined) {
            queryParameters['key'] = parameters['key'];
        }

        /** set default value **/
        queryParameters['isChina'] = -1;

        if (parameters['isChina'] !== undefined) {
            queryParameters['isChina'] = parameters['isChina'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 删除评价
     * @method
     * @name ServiceStore#DeleteEvaluateById
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.DeleteEvaluateById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Evaluate/DeleteEvaluateById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 回复评价
     * @method
     * @name ServiceStore#AddEvaluate
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.AddEvaluate = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Evaluate/AddEvaluate';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取评价列表
     * @method
     * @name ServiceStore#GetEvaluatePagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {string} parameters.orderNo - 订单号
     * @param {string} parameters.starTime - 开始评价时间
     * @param {string} parameters.endTime - 结束评价时间
     * @param {integer} parameters.status - 评价状态：0=待卖家评价，1=卖家已评价,-1=查询全部
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetEvaluatePagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Evaluate/GetEvaluatePagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        if (parameters['starTime'] !== undefined) {
            queryParameters['starTime'] = parameters['starTime'];
        }

        if (parameters['endTime'] !== undefined) {
            queryParameters['endTime'] = parameters['endTime'];
        }

        /** set default value **/
        queryParameters['status'] = -1;

        if (parameters['status'] !== undefined) {
            queryParameters['status'] = parameters['status'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加运费模板
     * @method
     * @name ServiceStore#AddFreightTemplate
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.AddFreightTemplate = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/AddFreightTemplate';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改运费模板
     * @method
     * @name ServiceStore#EditFreightTemplate
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditFreightTemplate = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/EditFreightTemplate';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 删除运费模板
     * @method
     * @name ServiceStore#EditFreightTemplateState
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditFreightTemplateState = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/EditFreightTemplateState';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取模板信息通过模板id
     * @method
     * @name ServiceStore#GetFreightTemplateById
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.ftId - 模板id
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetFreightTemplateById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/GetFreightTemplateById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['ftId'] !== undefined) {
            queryParameters['ftId'] = parameters['ftId'];
        }

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 运费模板与子模版分页列表
     * @method
     * @name ServiceStore#GetFreightTemplatePagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {string} parameters.keys - 搜索关键字
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetFreightTemplatePagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/GetFreightTemplatePagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['keys'] !== undefined) {
            queryParameters['keys'] = parameters['keys'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取运费模板
     * @method
     * @name ServiceStore#GetByProductPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {string} parameters.keys - 搜索关键字
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetByProductPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/FreightTemplate/GetByProductPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['keys'] !== undefined) {
            queryParameters['keys'] = parameters['keys'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取订单信息
     * @method
     * @name ServiceStore#GetOrderByOrderNo
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.orderNo - 订单编号
     */
    ServiceStore.prototype.GetOrderByOrderNo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/GetOrderByOrderNo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取订单列表
     * @method
     * @name ServiceStore#GetOrderPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {string} parameters.orderNo - 订单号
     * @param {integer} parameters.status - 订单状态
     * @param {string} parameters.keyWords - 商品名称搜素
     * @param {integer} parameters.source - 订单来源，-1=查询全部，0=PC商城，1=微信小程序商城
     * @param {string} parameters.startTime - 
     * @param {string} parameters.endTime - 结束交易时间
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetOrderPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/GetOrderPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        /** set default value **/
        queryParameters['status'] = -2;

        if (parameters['status'] !== undefined) {
            queryParameters['status'] = parameters['status'];
        }

        if (parameters['keyWords'] !== undefined) {
            queryParameters['keyWords'] = parameters['keyWords'];
        }

        /** set default value **/
        queryParameters['source'] = -1;

        if (parameters['source'] !== undefined) {
            queryParameters['source'] = parameters['source'];
        }

        if (parameters['startTime'] !== undefined) {
            queryParameters['startTime'] = parameters['startTime'];
        }

        if (parameters['endTime'] !== undefined) {
            queryParameters['endTime'] = parameters['endTime'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 订单发货
     * @method
     * @name ServiceStore#EditSendGoodsStatus
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 发货信息
     */
    ServiceStore.prototype.EditSendGoodsStatus = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/EditSendGoodsStatus';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改卖家备注
     * @method
     * @name ServiceStore#EditOrderSellerMessageByOrderNo
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 备注信息
     */
    ServiceStore.prototype.EditOrderSellerMessageByOrderNo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/EditOrderSellerMessageByOrderNo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 导出订单
     * @method
     * @name ServiceStore#GetOrderBatchPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {string} parameters.orderNo - 订单号
     * @param {integer} parameters.status - 订单状态
     * @param {string} parameters.starTime - 开始交易时间
     * @param {string} parameters.endTime - 结束交易时间
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetOrderBatchPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/GetOrderBatchPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        if (parameters['status'] !== undefined) {
            queryParameters['status'] = parameters['status'];
        }

        if (parameters['starTime'] !== undefined) {
            queryParameters['starTime'] = parameters['starTime'];
        }

        if (parameters['endTime'] !== undefined) {
            queryParameters['endTime'] = parameters['endTime'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 
     * @method
     * @name ServiceStore#DeclareOrder
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.orderNo - 
     */
    ServiceStore.prototype.DeclareOrder = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Order/DeclareOrder';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加角色
     * @method
     * @name ServiceStore#AddRole
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 输入model
     */
    ServiceStore.prototype.AddRole = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/AddRole';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改角色
     * @method
     * @name ServiceStore#EditRole
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 输入model
     */
    ServiceStore.prototype.EditRole = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/EditRole';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取所有菜单及功能
     * @method
     * @name ServiceStore#GetMenu
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.isDeleted - 是否删除，0=启用，1=禁用
     */
    ServiceStore.prototype.GetMenu = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetMenu';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        /** set default value **/
        queryParameters['isDeleted'] = -1;

        if (parameters['isDeleted'] !== undefined) {
            queryParameters['isDeleted'] = parameters['isDeleted'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取菜单以及功能，通过角色和父级id
     * @method
     * @name ServiceStore#GetMenuByRole
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.roleId - 角色id
     * @param {string} parameters.id - 菜单id
     * @param {boolean} parameters.isCach - 是否拿缓存
     */
    ServiceStore.prototype.GetMenuByRole = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetMenuByRole';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['roleId'] !== undefined) {
            queryParameters['roleId'] = parameters['roleId'];
        }

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        if (parameters['isCach'] !== undefined) {
            queryParameters['isCach'] = parameters['isCach'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 通过角色id下边的所有选中菜单
     * @method
     * @name ServiceStore#GetRoleMenuInfoByRoleId
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.roleId - 角色id
     */
    ServiceStore.prototype.GetRoleMenuInfoByRoleId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetRoleMenuInfoByRoleId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['roleId'] !== undefined) {
            queryParameters['roleId'] = parameters['roleId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 通过角色id获取要修改的角色下权限下的菜单和角色信息
     * @method
     * @name ServiceStore#GetEditRoleInfoByRoleId
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.roleId - 角色id
     */
    ServiceStore.prototype.GetEditRoleInfoByRoleId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetEditRoleInfoByRoleId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['roleId'] !== undefined) {
            queryParameters['roleId'] = parameters['roleId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 启用，禁用，物理删除 -角色
     * @method
     * @name ServiceStore#EditRoleState
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditRoleState = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/EditRoleState';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取角色列表
     * @method
     * @name ServiceStore#GetRoleList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetRoleList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetRoleList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取角色列表
     * @method
     * @name ServiceStore#GetRolePagedList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.storeId - 店铺id
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetRolePagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetRolePagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取绑定角色的用户列表
     * @method
     * @name ServiceStore#GetUserPagedListByRoleId
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.roleId - 
     * @param {integer} parameters.pageIndex - 
     * @param {integer} parameters.pageSize - 
     */
    ServiceStore.prototype.GetUserPagedListByRoleId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetUserPagedListByRoleId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['roleId'] !== undefined) {
            queryParameters['roleId'] = parameters['roleId'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 启用，禁用，物理删除 -用户绑定角色功能
     * @method
     * @name ServiceStore#EditUserByRoleState
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditUserByRoleState = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/EditUserByRoleState';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * [店家后台]获取当前用户所有菜单不包含按钮
     * @method
     * @name ServiceStore#GetRoleMenuByUserId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetRoleMenuByUserId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetRoleMenuByUserId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * [店家后台]根据当前角色进入菜单url，获取按钮权限
     * @method
     * @name ServiceStore#GetRoleMenuByMenuUrl
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     * @param {string} parameters.menuUrl - 
     */
    ServiceStore.prototype.GetRoleMenuByMenuUrl = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/GetRoleMenuByMenuUrl';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['menuUrl'] !== undefined) {
            queryParameters['menuUrl'] = parameters['menuUrl'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加菜单
     * @method
     * @name ServiceStore#AddMenu
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.AddMenu = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Permission/AddMenu';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加商品
     * @method
     * @name ServiceStore#AddProduct
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.AddProduct = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/AddProduct';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改商品
     * @method
     * @name ServiceStore#EditProduct
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditProduct = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/EditProduct';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 删除商品
     * @method
     * @name ServiceStore#DelProductById
     * @param {object} parameters - method options and parameters
     * @param {} parameters.delProductInput - 
     */
    ServiceStore.prototype.DelProductById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/DelProductById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['delProductInput'] !== undefined) {
            body = parameters['delProductInput'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取商品信息
     * @method
     * @name ServiceStore#GetProductById
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     * @param {integer} parameters.productId - 商品id
     */
    ServiceStore.prototype.GetProductById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/GetProductById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['productId'] !== undefined) {
            queryParameters['productId'] = parameters['productId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取商品分页列表
     * @method
     * @name ServiceStore#GetProductPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {integer} parameters.storeCategoryId - 分类id
     * @param {string} parameters.sn - 商品条形码
     * @param {string} parameters.merchantCode - 商品编码
     * @param {integer} parameters.shelfStatus - 商品状态 0上架 1下架 2全部
     * @param {integer} parameters.tradeType - 
     * @param {string} parameters.keyWords - 关键字检索
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetProductPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/GetProductPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['storeCategoryId'] !== undefined) {
            queryParameters['storeCategoryId'] = parameters['storeCategoryId'];
        }

        if (parameters['sn'] !== undefined) {
            queryParameters['sn'] = parameters['sn'];
        }

        if (parameters['merchantCode'] !== undefined) {
            queryParameters['merchantCode'] = parameters['merchantCode'];
        }

        /** set default value **/
        queryParameters['shelfStatus'] = 2;

        if (parameters['shelfStatus'] !== undefined) {
            queryParameters['shelfStatus'] = parameters['shelfStatus'];
        }

        /** set default value **/
        queryParameters['tradeType'] = -1;

        if (parameters['tradeType'] !== undefined) {
            queryParameters['tradeType'] = parameters['tradeType'];
        }

        if (parameters['keyWords'] !== undefined) {
            queryParameters['keyWords'] = parameters['keyWords'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 30;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 商品上下架
     * @method
     * @name ServiceStore#ProductShelfStatusById
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.ProductShelfStatusById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/ProductShelfStatusById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑商品权重
     * @method
     * @name ServiceStore#EditProductSortById
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditProductSortById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/EditProductSortById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取商品售后服务列表
     * @method
     * @name ServiceStore#GetProductAfterSaleService
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.backgroundCategoryId - 后台类目id
     */
    ServiceStore.prototype.GetProductAfterSaleService = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/GetProductAfterSaleService';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['backgroundCategoryId'] !== undefined) {
            queryParameters['backgroundCategoryId'] = parameters['backgroundCategoryId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取产品单位
     * @method
     * @name ServiceStore#GetProductUnitPagedList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.name - 单位模糊搜索
     * @param {integer} parameters.isDeleted - 是否删除，-1查询全部
     * @param {integer} parameters.pageIndex - 
     * @param {integer} parameters.pageSize - 
     */
    ServiceStore.prototype.GetProductUnitPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Product/GetProductUnitPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['name'] !== undefined) {
            queryParameters['name'] = parameters['name'];
        }

        /** set default value **/
        queryParameters['isDeleted'] = -1;

        if (parameters['isDeleted'] !== undefined) {
            queryParameters['isDeleted'] = parameters['isDeleted'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据查询项获取我的退款订单
     * @method
     * @name ServiceStore#GetMyRefundOrderPaged
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺Id
     * @param {integer} parameters.refundDetailType - 退款详细类型（0仅退款（未发货），1仅退款（已发货），2退货（已发货））
     * @param {string} parameters.refundOrderNo - 退款订单号
     * @param {string} parameters.orderNo - 订单号
     * @param {string} parameters.refundExpressNo - 退货运单号
     * @param {string} parameters.returnContactTel - 退货联系人电话
     * @param {string} parameters.merchantCode - 商户商品编号
     * @param {string} parameters.applyRefundStartTime - 退款发起时间开始
     * @param {string} parameters.applyRefundEndTime - 退款发起时间结束
     * @param {string} parameters.orderPayStartTime - 下单时间开始
     * @param {string} parameters.orderPayEndTime - 下单时间结束
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页长
     */
    ServiceStore.prototype.GetMyRefundOrderPaged = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/GetMyRefundOrderPaged';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['refundDetailType'] !== undefined) {
            queryParameters['refundDetailType'] = parameters['refundDetailType'];
        }

        if (parameters['refundOrderNo'] !== undefined) {
            queryParameters['refundOrderNo'] = parameters['refundOrderNo'];
        }

        if (parameters['orderNo'] !== undefined) {
            queryParameters['orderNo'] = parameters['orderNo'];
        }

        if (parameters['refundExpressNo'] !== undefined) {
            queryParameters['refundExpressNo'] = parameters['refundExpressNo'];
        }

        if (parameters['returnContactTel'] !== undefined) {
            queryParameters['returnContactTel'] = parameters['returnContactTel'];
        }

        if (parameters['merchantCode'] !== undefined) {
            queryParameters['merchantCode'] = parameters['merchantCode'];
        }

        if (parameters['applyRefundStartTime'] !== undefined) {
            queryParameters['applyRefundStartTime'] = parameters['applyRefundStartTime'];
        }

        if (parameters['applyRefundEndTime'] !== undefined) {
            queryParameters['applyRefundEndTime'] = parameters['applyRefundEndTime'];
        }

        if (parameters['orderPayStartTime'] !== undefined) {
            queryParameters['orderPayStartTime'] = parameters['orderPayStartTime'];
        }

        if (parameters['orderPayEndTime'] !== undefined) {
            queryParameters['orderPayEndTime'] = parameters['orderPayEndTime'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据退款订单号获取退款订单信息
     * @method
     * @name ServiceStore#GetRefundOrderByRefundOrderNo
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.refundOrderNo - 
     */
    ServiceStore.prototype.GetRefundOrderByRefundOrderNo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/GetRefundOrderByRefundOrderNo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['refundOrderNo'] !== undefined) {
            queryParameters['refundOrderNo'] = parameters['refundOrderNo'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据订单明细Id获取退款订单信息
     * @method
     * @name ServiceStore#GetRefundOrderByOrderDetailId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.orderDetailId - 订单明细Id
     */
    ServiceStore.prototype.GetRefundOrderByOrderDetailId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/GetRefundOrderByOrderDetailId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['orderDetailId'] !== undefined) {
            queryParameters['orderDetailId'] = parameters['orderDetailId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 同意售中退款
     * @method
     * @name ServiceStore#AgreeRefundInSale
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 售中退款输入
     */
    ServiceStore.prototype.AgreeRefundInSale = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/AgreeRefundInSale';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 同意售后退款
     * @method
     * @name ServiceStore#AgreeRefundAfterSale
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 售后退款输入
     */
    ServiceStore.prototype.AgreeRefundAfterSale = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/AgreeRefundAfterSale';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 拒绝售中退款并且发货
     * @method
     * @name ServiceStore#RejectRefundAndDeliverInSale
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 拒绝售中退款输入
     */
    ServiceStore.prototype.RejectRefundAndDeliverInSale = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/RejectRefundAndDeliverInSale';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 拒绝售后拒绝退款
     * @method
     * @name ServiceStore#RejectRefundAfterSale
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 拒绝售后退款输入
     */
    ServiceStore.prototype.RejectRefundAfterSale = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/RejectRefundAfterSale';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 同意退货
     * @method
     * @name ServiceStore#AgreeReturnGoods
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 同意退货输入
     */
    ServiceStore.prototype.AgreeReturnGoods = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/AgreeReturnGoods';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 拒绝退货
     * @method
     * @name ServiceStore#RejectReturnGoods
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 退款订单号
     */
    ServiceStore.prototype.RejectReturnGoods = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/RejectReturnGoods';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 新增留言记录
     * @method
     * @name ServiceStore#AddLeavingMessage
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 新增留言输入
     */
    ServiceStore.prototype.AddLeavingMessage = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/AddLeavingMessage';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 退款订单申请平台介入
     * @method
     * @name ServiceStore#ApplyForRefundPlatformIntervene
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 店铺申请平台介入输入
     */
    ServiceStore.prototype.ApplyForRefundPlatformIntervene = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/ApplyForRefundPlatformIntervene';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据退款号获取退款进度信息
     * @method
     * @name ServiceStore#GetRefundOrderPayInfo
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.refundOrderNo - 退款订单号
     */
    ServiceStore.prototype.GetRefundOrderPayInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/GetRefundOrderPayInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['refundOrderNo'] !== undefined) {
            queryParameters['refundOrderNo'] = parameters['refundOrderNo'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 平台介入提交凭证
     * @method
     * @name ServiceStore#SubmitPlatformInterveneProof
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 店铺提交平台介入凭证
     */
    ServiceStore.prototype.SubmitPlatformInterveneProof = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/SubmitPlatformInterveneProof';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 平台介入店铺查看凭证后拒绝
     * @method
     * @name ServiceStore#RejectPlatformInterveneProof
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 查看凭证后拒绝输入
     */
    ServiceStore.prototype.RejectPlatformInterveneProof = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/RejectPlatformInterveneProof';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取订单拒绝原因(包括拒绝退款/拒绝退货)
     * @method
     * @name ServiceStore#GetOrderRejectRefundReasons
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.categoryId - 分类Id
     * @param {integer} parameters.refundType - 退款类型
     * @param {integer} parameters.orderExpressStatus - 货物状态
     * @param {integer} parameters.refundReasonType - 退款原因类型
     */
    ServiceStore.prototype.GetOrderRejectRefundReasons = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/RefundOrder/GetOrderRejectRefundReasons';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['categoryId'] !== undefined) {
            queryParameters['categoryId'] = parameters['categoryId'];
        }

        if (parameters['refundType'] !== undefined) {
            queryParameters['refundType'] = parameters['refundType'];
        }

        if (parameters['orderExpressStatus'] !== undefined) {
            queryParameters['orderExpressStatus'] = parameters['orderExpressStatus'];
        }

        if (parameters['refundReasonType'] !== undefined) {
            queryParameters['refundReasonType'] = parameters['refundReasonType'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加员工
     * @method
     * @name ServiceStore#AddStaff
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.AddStaff = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/AddStaff';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑员工
     * @method
     * @name ServiceStore#EditStaff
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditStaff = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/EditStaff';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取员工列表
     * @method
     * @name ServiceStore#GetStaffPagedList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.storeId - 店铺id
     * @param {string} parameters.keyWords - 
     * @param {integer} parameters.pageIndex - 页码
     * @param {integer} parameters.pageSize - 页尺寸
     */
    ServiceStore.prototype.GetStaffPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/GetStaffPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['keyWords'] !== undefined) {
            queryParameters['keyWords'] = parameters['keyWords'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 启用，禁用，物理删除 -员工
     * @method
     * @name ServiceStore#EditStaffState
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditStaffState = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/EditStaffState';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 通过员工id和用户获取员工信息
     * @method
     * @name ServiceStore#GetEditStaffInfoByUserId
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.uaugId - 用户组与用户关联id(员工id)
     * @param {integer} parameters.userId - 用户id
     */
    ServiceStore.prototype.GetEditStaffInfoByUserId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/GetEditStaffInfoByUserId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['uaugId'] !== undefined) {
            queryParameters['uaugId'] = parameters['uaugId'];
        }

        if (parameters['userId'] !== undefined) {
            queryParameters['userId'] = parameters['userId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改员工密码
     * @method
     * @name ServiceStore#EditPassword
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditPassword = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Staff/EditPassword';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 注册店铺
     * @method
     * @name ServiceStore#AddStore
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.AddStore = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/AddStore';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑店铺
     * @method
     * @name ServiceStore#EditStore
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditStore = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/EditStore';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑店铺
     * @method
     * @name ServiceStore#EditStoreName
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditStoreName = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/EditStoreName';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑店铺
     * @method
     * @name ServiceStore#EditStoreLogo
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditStoreLogo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/EditStoreLogo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 店铺列表
     * @method
     * @name ServiceStore#GetStoreList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeType - 
     * @param {string} parameters.storeName - 
     * @param {integer} parameters.pageIndex - 
     * @param {integer} parameters.pageSize - 
     */
    ServiceStore.prototype.GetStoreList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/GetStoreList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        /** set default value **/
        queryParameters['storeType'] = -1;

        if (parameters['storeType'] !== undefined) {
            queryParameters['storeType'] = parameters['storeType'];
        }

        if (parameters['storeName'] !== undefined) {
            queryParameters['storeName'] = parameters['storeName'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据店铺id获取店铺详情
     * @method
     * @name ServiceStore#GetStoreDetail
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetStoreDetail = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/GetStoreDetail';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺负责人信息
     * @method
     * @name ServiceStore#GetStoreContactByStoreId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetStoreContactByStoreId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/GetStoreContactByStoreId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺基本信息
     * @method
     * @name ServiceStore#GetStoreBaseInfoByStoreId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetStoreBaseInfoByStoreId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/Store/GetStoreBaseInfoByStoreId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺认证信息
     * @method
     * @name ServiceStore#GetAuthInfo
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetAuthInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/GetAuthInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺主体认证信息
     * @method
     * @name ServiceStore#GetSubjectAuthInfo
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetSubjectAuthInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/GetSubjectAuthInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 跨境电商认证并修改店铺主表认证状态
     * @method
     * @name ServiceStore#AddStoreCrossBorderAuth
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.AddStoreCrossBorderAuth = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/AddStoreCrossBorderAuth';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 跨境电商认证修改并修改店铺主表认证状态
     * @method
     * @name ServiceStore#EditStoreCrossBorderAuth
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditStoreCrossBorderAuth = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/EditStoreCrossBorderAuth';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取跨境电商认证信息
     * @method
     * @name ServiceStore#GetCrossBorderAuthInfo
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetCrossBorderAuthInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/GetCrossBorderAuthInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加店铺资料+主体认证并修改店铺主表认证状态
     * @method
     * @name ServiceStore#AddStoreOrSubjectAuth
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 店铺和主体实体
     */
    ServiceStore.prototype.AddStoreOrSubjectAuth = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/AddStoreOrSubjectAuth';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改店铺资料+主体认证并修改店铺主表认证状态
     * @method
     * @name ServiceStore#EditStoreOrSubjectAuth
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 店铺和主体实体
     */
    ServiceStore.prototype.EditStoreOrSubjectAuth = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/EditStoreOrSubjectAuth';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺认证信息--包含店铺，主体
     * @method
     * @name ServiceStore#GetStoreOrSubjectAuthInfo
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetStoreOrSubjectAuthInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/GetStoreOrSubjectAuthInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取认证记录
     * @method
     * @name ServiceStore#GetStoreAuthRecordListByAuthId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     * @param {integer} parameters.authModule - 认证模块标识：0店铺，1主体，2跨境
     * @param {integer} parameters.authId - 认证id
     */
    ServiceStore.prototype.GetStoreAuthRecordListByAuthId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreAuth/GetStoreAuthRecordListByAuthId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['authModule'] !== undefined) {
            queryParameters['authModule'] = parameters['authModule'];
        }

        if (parameters['authId'] !== undefined) {
            queryParameters['authId'] = parameters['authId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加商铺分类
     * @method
     * @name ServiceStore#AddStoreCategory
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.AddStoreCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreCategory/AddStoreCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改商铺分类
     * @method
     * @name ServiceStore#EditStoreCategory
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditStoreCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreCategory/EditStoreCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 启用，禁用，物理删除 -商铺分类
     * @method
     * @name ServiceStore#EditStoreCategoryState
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 传入model
     */
    ServiceStore.prototype.EditStoreCategoryState = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreCategory/EditStoreCategoryState';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取商铺分类，通过店铺id获取
     * @method
     * @name ServiceStore#GetStoreCategory
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 通过店铺id
     * @param {boolean} parameters.isCach - 
     */
    ServiceStore.prototype.GetStoreCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreCategory/GetStoreCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['isCach'] !== undefined) {
            queryParameters['isCach'] = parameters['isCach'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 店铺联系我们-添加店铺联系地址
     * @method
     * @name ServiceStore#AddOrEditStoreContactAddress
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.AddOrEditStoreContactAddress = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreContactAddress/AddOrEditStoreContactAddress';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 店铺联系我们-根据StoreId查询店铺联系地址
     * @method
     * @name ServiceStore#GetStoreContactAddressByStoreId
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetStoreContactAddressByStoreId = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreContactAddress/GetStoreContactAddressByStoreId';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺首页销售报表
     * @method
     * @name ServiceStore#GetHomeSaleReport
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetHomeSaleReport = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreHome/GetHomeSaleReport';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺首页销售汇总
     * @method
     * @name ServiceStore#GetHomeSale
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetHomeSale = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreHome/GetHomeSale';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 店铺退货地址列表分页
     * @method
     * @name ServiceStore#GetStoreReturnAddressPagedList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     * @param {string} parameters.addressAlias - 
     * @param {string} parameters.companyName - 
     * @param {string} parameters.contactsName - 
     * @param {string} parameters.phone - 
     * @param {integer} parameters.pageIndex - 
     * @param {integer} parameters.pageSize - 
     */
    ServiceStore.prototype.GetStoreReturnAddressPagedList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/GetStoreReturnAddressPagedList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        if (parameters['addressAlias'] !== undefined) {
            queryParameters['addressAlias'] = parameters['addressAlias'];
        }

        if (parameters['companyName'] !== undefined) {
            queryParameters['companyName'] = parameters['companyName'];
        }

        if (parameters['contactsName'] !== undefined) {
            queryParameters['contactsName'] = parameters['contactsName'];
        }

        if (parameters['phone'] !== undefined) {
            queryParameters['phone'] = parameters['phone'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 店铺退货地址列表全部
     * @method
     * @name ServiceStore#GetStoreReturnAddressList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetStoreReturnAddressList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/GetStoreReturnAddressList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加店铺退货地址
     * @method
     * @name ServiceStore#AddStoreReturnAddress
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.AddStoreReturnAddress = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/AddStoreReturnAddress';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 编辑店铺退货地址
     * @method
     * @name ServiceStore#EditStoreReturnAddress
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.EditStoreReturnAddress = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/EditStoreReturnAddress';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 删除店铺退货地址
     * @method
     * @name ServiceStore#DelStoreReturnAddress
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServiceStore.prototype.DelStoreReturnAddress = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/DelStoreReturnAddress';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据id查询退货地址
     * @method
     * @name ServiceStore#GetStoreReturnAddressByIdAsync
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.sraId - 
     * @param {integer} parameters.storeId - 
     */
    ServiceStore.prototype.GetStoreReturnAddressByIdAsync = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreReturnAddress/GetStoreReturnAddressByIdAsync';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['sraId'] !== undefined) {
            queryParameters['sraId'] = parameters['sraId'];
        }

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取店铺设置
     * @method
     * @name ServiceStore#GetStoreSetting
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.storeId - 店铺id
     */
    ServiceStore.prototype.GetStoreSetting = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreSetting/GetStoreSetting';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['storeId'] !== undefined) {
            queryParameters['storeId'] = parameters['storeId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 修改店铺设置
     * @method
     * @name ServiceStore#EditStoreSetting
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 修改店铺输入
     */
    ServiceStore.prototype.EditStoreSetting = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/StoreSetting/EditStoreSetting';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取用户信息
     * @method
     * @name ServiceStore#GetUserInfo
     * @param {object} parameters - method options and parameters
     */
    ServiceStore.prototype.GetUserInfo = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/User/GetUserInfo';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 统一下单支付结果通知
     * @method
     * @name ServiceStore#Unifiedorder
     * @param {object} parameters - method options and parameters
     */
    ServiceStore.prototype.Unifiedorder = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Store/WeChatPayNotify/Unifiedorder';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };

    return ServiceStore;
})();

export default ServiceStore;