let serverConfig = {
  servicePublic: 'https://dev-public-api.kjdserp.com',
  serviceStore: 'https://dev-store-api.kjdserp.com',
  serviceUpload: 'https://dev-file-api.kjdserp.com/files/',
}

if (process.env.target == 'dev') {
  Object.assign(serverConfig, {})
}
export {
  serverConfig
}