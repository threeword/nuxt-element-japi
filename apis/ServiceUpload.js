/*jshint esversion: 6 */
/*global fetch, btoa */
import Q from 'q';
import {superagentRequest} from "./superagentRequest";
/**
 * ## 统一约定
1. 所有API均采用RESTful架构设计，请注意使用的HTTP方法
2. request/response字符编码统一采用UTF8编码
3. 为方便描述，接口地中的{英文字母}为参数占位符
4. 所有API返回结果均为JSON格式，其通用字段如下：
		{
			"errCode": 1,
			"errMsg": "失败时的错误消息",
			"data": {
				//成功时的返回数据
				…
			}
		}
5. API返回结果中的errCode，100及以上特定接口有的特定涵义，100以下为通用错误码，定义如下：

errCode	|	说明
-------	|	----------
0		|	接口调用成功或执行成功				
1		|	未知错误						
2		|	接口调用失败或执行失败			
3		|	接口调用失败（传入接口参数有错误）
4		|	接口调用失败（服务器内部有错误）
5		|	接口调用失败（未授权）


# 快速入门
## 上传文件

首先调用上传文件接口，成功会返回如下格式的JSON：
```javascript
{
  "data": {
    ...
    "url": "http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--",
    ...
  },
  "errCode": 0
}
```
url字段为文件根地址（用法稍后有说明）


## 下载文件

文件根地址不可直接访问，需要拼接URL后才能访问，不同拼接方法有不同的作用。
下面举例说明：

### 下载原文件
文件根地址/raw，例如：
http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/raw

### 下载128x128大小的缩略图（原文件是图像）
文件根地址/image/128x128，例如：
http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128

### 下载128宽，高等比缩放的缩略图（原文件是图像）
文件根地址/image/128x，例如：
http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x

### 原图是JPG格式，下载png格式的图像
文件根地址/image/raw_png，例如：
http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/raw_png

### 原图是JPG格式，下载png格式的128x128大小的缩略像
文件根地址/image/128x128_png，例如：
http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128_png

以上只是基本用法的简单举例，具体支持的高级用法请查看在线文档【GET /files/handlers/{name} 获取指定文件处理器支持的转换修饰符信息】部分


## 使用建议

* 对于图像由于客户端屏幕分辨率、网络环境等差异，实际需要显示的大小也不同，建议客户端自行选择合适的大小拼接后缀。
  例如：产品列表页显示产品缩略图时只需要拼接合适大小的缩略图即可，而不必请求原图，原图通常比较大，浪费流量同时也增加加载时间。

 * @class ServiceUpload
 * @param {(string|object)} [domainOrOptions] - The project domain or options object. If object, see the object's optional properties.
 * @param {string} [domainOrOptions.domain] - The project domain
 * @param {object} [domainOrOptions.token] - auth token - object with value property and optional headerOrQueryName and isQuery properties
 */
let ServiceUpload = (function() {
    'use strict';

    function ServiceUpload(options) {
        let domain = (typeof options === 'object') ? options.domain : options;
        this.domain = domain ? domain : '';
        if (this.domain.length === 0) {
            throw new Error('Domain parameter must be specified as a string.');
        }
    }

    function serializeQueryParams(parameters) {
        let str = [];
        for (let p in parameters) {
            if (parameters.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent(parameters[p]));
            }
        }
        return str.join('&');
    }

    function mergeQueryParams(parameters, queryParameters) {
        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    let parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }
        return queryParameters;
    }

    /**
     * HTTP Request
     * @method
     * @name ServiceUpload#request
     * @param {string} method - http method
     * @param {string} url - url to do request
     * @param {object} parameters
     * @param {object} body - body parameters / object
     * @param {object} headers - header parameters
     * @param {object} queryParameters - querystring parameters
     * @param {object} form - form data object
     * @param {object} deferred - promise object
     */
    ServiceUpload.prototype.request = function(method, url, parameters, body, headers, queryParameters, form, deferred) {
        const queryParams = queryParameters && Object.keys(queryParameters).length ? serializeQueryParams(queryParameters) : null;
        const urlWithParams = url + (queryParams ? '?' + queryParams : '');

        if (body && !Object.keys(body).length) {
            body = undefined;
        }

        fetch(urlWithParams, {
            method,
            headers,
            body: JSON.stringify(body)
        }).then((response) => {
            return response.json();
        }).then((body) => {
            deferred.resolve(body);
        }).catch((error) => {
            deferred.reject(error);
        });
    };

    /**
     * ### 上传步骤：
    1. 在真正上传文件前先计算文件SHA1值
    2. 调用本接口只传hash参数，不传file参数
    3. 如果本文件之前有人传过则接口返回成功(errCode=0)
       1. 跳过后续步骤
    4. 如果本文件之前没人传过，则errCode=100
       1. 再次调用本接口，传file参数，不传hash参数（传也会忽略）
     * @method
     * @name ServiceUpload#FilesUploadPost
     * @param {object} parameters - method options and parameters
         * @param {string} parameters.fileName - 文件名（包含扩展名），不传则从文件流中读取。例如：test.jpg
         * @param {file} parameters.file - 待上传的文件流
         * @param {string} parameters.hash - 待上传文件的SHA1值。例如：c64376a0d4677f0d4df563fe23f0c8239a45c17d
         * @param {integer} parameters.periodMinute - 链接有效期（分钟）,0则不过期
         * @param {string} parameters.imageWatermarkText - ## 统一约定
    1. 所有API均采用RESTful架构设计，请注意使用的HTTP方法
    2. request/response字符编码统一采用UTF8编码
    3. 为方便描述，接口地中的{英文字母}为参数占位符
    4. 所有API返回结果均为JSON格式，其通用字段如下：
    		{
    			"errCode": 1,
    			"errMsg": "失败时的错误消息",
    			"data": {
    				//成功时的返回数据
    				…
    			}
    		}
    5. API返回结果中的errCode，100及以上特定接口有的特定涵义，100以下为通用错误码，定义如下：

    errCode	|	说明
    -------	|	----------
    0		|	接口调用成功或执行成功				
    1		|	未知错误						
    2		|	接口调用失败或执行失败			
    3		|	接口调用失败（传入接口参数有错误）
    4		|	接口调用失败（服务器内部有错误）
    5		|	接口调用失败（未授权）


    # 快速入门
    ## 上传文件

    首先调用上传文件接口，成功会返回如下格式的JSON：
    ```javascript
    {
      "data": {
        ...
        "url": "http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--",
        ...
      },
      "errCode": 0
    }
    ```
    url字段为文件根地址（用法稍后有说明）


    ## 下载文件

    文件根地址不可直接访问，需要拼接URL后才能访问，不同拼接方法有不同的作用。
    下面举例说明：

    ### 下载原文件
    文件根地址/raw，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/raw

    ### 下载128x128大小的缩略图（原文件是图像）
    文件根地址/image/128x128，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128

    ### 下载128宽，高等比缩放的缩略图（原文件是图像）
    文件根地址/image/128x，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x

    ### 原图是JPG格式，下载png格式的图像
    文件根地址/image/raw_png，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/raw_png

    ### 原图是JPG格式，下载png格式的128x128大小的缩略像
    文件根地址/image/128x128_png，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128_png

    以上只是基本用法的简单举例，具体支持的高级用法请查看在线文档【GET /files/handlers/{name} 获取指定文件处理器支持的转换修饰符信息】部分


    ## 使用建议

    * 对于图像由于客户端屏幕分辨率、网络环境等差异，实际需要显示的大小也不同，建议客户端自行选择合适的大小拼接后缀。
      例如：产品列表页显示产品缩略图时只需要拼接合适大小的缩略图即可，而不必请求原图，原图通常比较大，浪费流量同时也增加加载时间。

         * @param {integer} parameters.imageWatermarkWatermarkType - 
    0 - Local | 1 - Fill
         * @param {integer} parameters.imageWatermarkWatermarkLocation - 
    0 - UpperLeft | 1 - UpperRight | 2 - LeftLower | 3 - UpperLower | 4 - Center
         * @param {string} parameters.ownerToken - 文件所有者令牌，此参数通过其它服务获取
     */
    ServiceUpload.prototype.FilesUploadPost = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/Upload';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['fileName'] !== undefined) {
            form['fileName'] = parameters['fileName'];
        }

        if (parameters['file'] !== undefined) {
            form['file'] = parameters['file'];
        }

        if (parameters['hash'] !== undefined) {
            form['hash'] = parameters['hash'];
        }

        if (parameters['periodMinute'] !== undefined) {
            form['periodMinute'] = parameters['periodMinute'];
        }

        if (parameters['imageWatermarkText'] !== undefined) {
            form['imageWatermark.Text'] = parameters['imageWatermarkText'];
        }

        if (parameters['imageWatermarkWatermarkType'] !== undefined) {
            form['imageWatermark.WatermarkType'] = parameters['imageWatermarkWatermarkType'];
        }

        if (parameters['imageWatermarkWatermarkType'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: imageWatermarkWatermarkType'));
            return deferred.promise;
        }

        if (parameters['imageWatermarkWatermarkLocation'] !== undefined) {
            form['imageWatermark.WatermarkLocation'] = parameters['imageWatermarkWatermarkLocation'];
        }

        if (parameters['imageWatermarkWatermarkLocation'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: imageWatermarkWatermarkLocation'));
            return deferred.promise;
        }

        if (parameters['ownerToken'] !== undefined) {
            form['ownerToken'] = parameters['ownerToken'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * ### 上传步骤：
    1. 在真正上传文件前先计算文件SHA1值
    2. 调用本接口只传hash参数，不传file参数
    3. 如果本文件之前有人传过则接口返回成功(errCode=0)
       1. 跳过后续步骤
    4. 如果本文件之前没人传过，则errCode=100
       1. 再次调用本接口，传file参数，不传hash参数（传也会忽略）
     * @method
     * @name ServiceUpload#FilesBatchUploadPost
     * @param {object} parameters - method options and parameters
         * @param {array} parameters.files - 待上传的文件流
         * @param {integer} parameters.periodMinute - 链接有效期（分钟）,0则不过期
         * @param {string} parameters.ownerToken - 文件所有者令牌，此参数通过其它服务获取
     */
    ServiceUpload.prototype.FilesBatchUploadPost = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/BatchUpload';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['files'] !== undefined) {
            queryParameters['files'] = parameters['files'];
        }

        if (parameters['periodMinute'] !== undefined) {
            form['periodMinute'] = parameters['periodMinute'];
        }

        if (parameters['ownerToken'] !== undefined) {
            form['ownerToken'] = parameters['ownerToken'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * UploadByRemote - 上传文件（从远程拉取）
     * @method
     * @name ServiceUpload#FilesFromRemotePost
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.fileName - 文件名（包含扩展名），不传则从文件流中读取。例如：test.jpg
     * @param {string} parameters.fileUrl - 文件下载地址
     * @param {integer} parameters.periodMinute - 链接有效期（分钟）,0则不过期
     * @param {string} parameters.ownerToken - 文件所有者令牌，此参数通过其它服务获取
     */
    ServiceUpload.prototype.FilesFromRemotePost = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/fromRemote';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        if (parameters['fileName'] !== undefined) {
            form['fileName'] = parameters['fileName'];
        }

        if (parameters['fileUrl'] !== undefined) {
            form['fileUrl'] = parameters['fileUrl'];
        }

        if (parameters['periodMinute'] !== undefined) {
            form['periodMinute'] = parameters['periodMinute'];
        }

        if (parameters['ownerToken'] !== undefined) {
            form['ownerToken'] = parameters['ownerToken'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * GetHandlers - 获取文件处理器列表
     * @method
     * @name ServiceUpload#FilesHandlersGet
     * @param {object} parameters - method options and parameters
     */
    ServiceUpload.prototype.FilesHandlersGet = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/handlers';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 不同处理器返回结构不同。
    花括号（{xxx}）包围的表示必选参数，方括号（[xxx]）包围的表示可选参数
     * @method
     * @name ServiceUpload#FilesHandlersByNameGet
     * @param {object} parameters - method options and parameters
         * @param {string} parameters.name - ## 统一约定
    1. 所有API均采用RESTful架构设计，请注意使用的HTTP方法
    2. request/response字符编码统一采用UTF8编码
    3. 为方便描述，接口地中的{英文字母}为参数占位符
    4. 所有API返回结果均为JSON格式，其通用字段如下：
    		{
    			"errCode": 1,
    			"errMsg": "失败时的错误消息",
    			"data": {
    				//成功时的返回数据
    				…
    			}
    		}
    5. API返回结果中的errCode，100及以上特定接口有的特定涵义，100以下为通用错误码，定义如下：

    errCode	|	说明
    -------	|	----------
    0		|	接口调用成功或执行成功				
    1		|	未知错误						
    2		|	接口调用失败或执行失败			
    3		|	接口调用失败（传入接口参数有错误）
    4		|	接口调用失败（服务器内部有错误）
    5		|	接口调用失败（未授权）


    # 快速入门
    ## 上传文件

    首先调用上传文件接口，成功会返回如下格式的JSON：
    ```javascript
    {
      "data": {
        ...
        "url": "http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--",
        ...
      },
      "errCode": 0
    }
    ```
    url字段为文件根地址（用法稍后有说明）


    ## 下载文件

    文件根地址不可直接访问，需要拼接URL后才能访问，不同拼接方法有不同的作用。
    下面举例说明：

    ### 下载原文件
    文件根地址/raw，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/raw

    ### 下载128x128大小的缩略图（原文件是图像）
    文件根地址/image/128x128，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128

    ### 下载128宽，高等比缩放的缩略图（原文件是图像）
    文件根地址/image/128x，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x

    ### 原图是JPG格式，下载png格式的图像
    文件根地址/image/raw_png，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/raw_png

    ### 原图是JPG格式，下载png格式的128x128大小的缩略像
    文件根地址/image/128x128_png，例如：
    http://file.domain.com/files/1iYQTU7fEUgaa~URSVwaCqQKFml_IAAAAAgAAAAbhmsFjiUUQwCPn2ngI1QcvsSp0AA--/image/128x128_png

    以上只是基本用法的简单举例，具体支持的高级用法请查看在线文档【GET /files/handlers/{name} 获取指定文件处理器支持的转换修饰符信息】部分


    ## 使用建议

    * 对于图像由于客户端屏幕分辨率、网络环境等差异，实际需要显示的大小也不同，建议客户端自行选择合适的大小拼接后缀。
      例如：产品列表页显示产品缩略图时只需要拼接合适大小的缩略图即可，而不必请求原图，原图通常比较大，浪费流量同时也增加加载时间。

     */
    ServiceUpload.prototype.FilesHandlersByNameGet = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/handlers/{name}';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        path = path.replace('{name}', parameters['name']);

        if (parameters['name'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: name'));
            return deferred.promise;
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * Download - 下载文件（不指定转换修饰符）
     * @method
     * @name ServiceUpload#FilesByFileTokenByHandlerGet
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.fileToken - <seealso cref="M:Mondol.FileService.Controllers.UserFileController.DownloadAsync(System.String,System.String,System.String)" />
     * @param {string} parameters.handler - <seealso cref="M:Mondol.FileService.Controllers.UserFileController.DownloadAsync(System.String,System.String,System.String)" />
     */
    ServiceUpload.prototype.FilesByFileTokenByHandlerGet = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/{fileToken}/{handler}';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        path = path.replace('{fileToken}', parameters['fileToken']);

        if (parameters['fileToken'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: fileToken'));
            return deferred.promise;
        }

        path = path.replace('{handler}', parameters['handler']);

        if (parameters['handler'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: handler'));
            return deferred.promise;
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * ### 图片转换实现：
    1. 假设fileToken=8o2YcHjzP-LYDNRdePMrxwE01WcbAAAAAQAAAAHhmsFjiPWwMe-XlA8.，对应的原文件为600x800的一张jpg图片        
    2. 您可以用文件根地址+sizes+formats拼接URL地址以实现转换的目的  
    3. 如果您想请求128x128，png格式的的图片可拼接为：http://file.mondol.info/files/8o2YcHjzP-LYDNRdePMrxwE01WcbAAAAAQAAAAHhmsFjiPWwMe-XlA8./128x128_png
     * @method
     * @name ServiceUpload#FilesByFileTokenByHandlerByModifierGet
     * @param {object} parameters - method options and parameters
         * @param {string} parameters.fileToken - 上传接口返回的文件访问令牌
         * @param {string} parameters.handler - 文件转换处理器名字
         * @param {string} parameters.modifier - 文件转换修饰符，定义参见对应文件处理器的描述
     */
    ServiceUpload.prototype.FilesByFileTokenByHandlerByModifierGet = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/files/{fileToken}/{handler}/{modifier}';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/x-www-form-urlencoded'];

        path = path.replace('{fileToken}', parameters['fileToken']);

        if (parameters['fileToken'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: fileToken'));
            return deferred.promise;
        }

        path = path.replace('{handler}', parameters['handler']);

        if (parameters['handler'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: handler'));
            return deferred.promise;
        }

        path = path.replace('{modifier}', parameters['modifier']);

        if (parameters['modifier'] === undefined) {
            deferred.reject(new Error('Missing required  parameter: modifier'));
            return deferred.promise;
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };

    return ServiceUpload;
})();

export default ServiceUpload;