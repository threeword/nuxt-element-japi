/*jshint esversion: 6 */
/*global fetch, btoa */
import Q from 'q';
import {superagentRequest} from "./superagentRequest";
/**
 * 
 * @class ServicePublic
 * @param {(string|object)} [domainOrOptions] - The project domain or options object. If object, see the object's optional properties.
 * @param {string} [domainOrOptions.domain] - The project domain
 * @param {object} [domainOrOptions.token] - auth token - object with value property and optional headerOrQueryName and isQuery properties
 */
let ServicePublic = (function() {
    'use strict';

    function ServicePublic(options) {
        let domain = (typeof options === 'object') ? options.domain : options;
        this.domain = domain ? domain : '';
        if (this.domain.length === 0) {
            throw new Error('Domain parameter must be specified as a string.');
        }
        this.apiKey = (typeof options === 'object') ? (options.apiKey ? options.apiKey : {}) : {};
    }

    function serializeQueryParams(parameters) {
        let str = [];
        for (let p in parameters) {
            if (parameters.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent(parameters[p]));
            }
        }
        return str.join('&');
    }

    function mergeQueryParams(parameters, queryParameters) {
        if (parameters.$queryParameters) {
            Object.keys(parameters.$queryParameters)
                .forEach(function(parameterName) {
                    let parameter = parameters.$queryParameters[parameterName];
                    queryParameters[parameterName] = parameter;
                });
        }
        return queryParameters;
    }

    /**
     * HTTP Request
     * @method
     * @name ServicePublic#request
     * @param {string} method - http method
     * @param {string} url - url to do request
     * @param {object} parameters
     * @param {object} body - body parameters / object
     * @param {object} headers - header parameters
     * @param {object} queryParameters - querystring parameters
     * @param {object} form - form data object
     * @param {object} deferred - promise object
     */
    ServicePublic.prototype.request = function(method, url, parameters, body, headers, queryParameters, form, deferred) {
        const queryParams = queryParameters && Object.keys(queryParameters).length ? serializeQueryParams(queryParameters) : null;
        const urlWithParams = url + (queryParams ? '?' + queryParams : '');

        if (body && !Object.keys(body).length) {
            body = undefined;
        }

        fetch(urlWithParams, {
            method,
            headers,
            body: JSON.stringify(body)
        }).then((response) => {
            return response.json();
        }).then((body) => {
            deferred.resolve(body);
        }).catch((error) => {
            deferred.reject(error);
        });
    };

    /**
     * Set Api Key
     * @method
     * @name ServicePublic#setApiKey
     * @param {string} value - apiKey's value
     * @param {string} headerOrQueryName - the header or query name to send the apiKey at
     * @param {boolean} isQuery - true if send the apiKey as query param, otherwise, send as header param
     */
    ServicePublic.prototype.setApiKey = function(value, headerOrQueryName, isQuery) {
        this.apiKey.value = value;
        this.apiKey.headerOrQueryName = headerOrQueryName;
        this.apiKey.isQuery = isQuery;
    };
    /**
     * Set Auth headers
     * @method
     * @name ServicePublic#setAuthHeaders
     * @param {object} headerParams - headers object
     */
    ServicePublic.prototype.setAuthHeaders = function(headerParams) {
        let headers = headerParams ? headerParams : {};
        if (!this.apiKey.isQuery && this.apiKey.headerOrQueryName) {
            headers[this.apiKey.headerOrQueryName] = this.apiKey.value;
        }
        return headers;
    };

    /**
     * 协议查询
     * @method
     * @name ServicePublic#GetAgreement
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.applyId - 
     * @param {integer} parameters.id - 
     * @param {integer} parameters.type - 
     */
    ServicePublic.prototype.GetAgreement = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Agreement/GetAgreement';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['applyId'] !== undefined) {
            queryParameters['applyId'] = parameters['applyId'];
        }

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        if (parameters['type'] !== undefined) {
            queryParameters['type'] = parameters['type'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取子级集合
     * @method
     * @name ServicePublic#GetArea
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.parentId - 
     */
    ServicePublic.prototype.GetArea = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Area/GetArea';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        /** set default value **/
        queryParameters['parentId'] = 142;

        if (parameters['parentId'] !== undefined) {
            queryParameters['parentId'] = parameters['parentId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取销售类目，通过父级获取
     * @method
     * @name ServicePublic#GetFrontDeskCategory
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.parentId - 
     */
    ServicePublic.prototype.GetFrontDeskCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Category/GetFrontDeskCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['parentId'] !== undefined) {
            queryParameters['parentID'] = parameters['parentId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取主营类目,通过父级获取
     * @method
     * @name ServicePublic#GetStoreMainCategory
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.parent - 父级类目id
     */
    ServicePublic.prototype.GetStoreMainCategory = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Category/GetStoreMainCategory';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['parent'] !== undefined) {
            queryParameters['parent'] = parameters['parent'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取销售类目id
     * @method
     * @name ServicePublic#FrontDeskCategoryById
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.id - 销售类目id
     * @param {integer} parameters.platformType - 类型：0=pc,1=小程序
     */
    ServicePublic.prototype.FrontDeskCategoryById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Category/FrontDeskCategoryById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        if (parameters['platformType'] !== undefined) {
            queryParameters['platformType'] = parameters['platformType'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取国家信息
     * @method
     * @name ServicePublic#GetCountrys
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.key - 关键字搜索
     */
    ServicePublic.prototype.GetCountrys = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Country/GetCountrys';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['key'] !== undefined) {
            queryParameters['key'] = parameters['key'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取数据字典
     * @method
     * @name ServicePublic#GetDictionaryByName
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.name - 数据名
     */
    ServicePublic.prototype.GetDictionaryByName = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Dictionary/GetDictionaryByName';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['name'] !== undefined) {
            queryParameters['name'] = parameters['name'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 查询所有文档菜单
     * @method
     * @name ServicePublic#GetDocumentMenuList
     * @param {object} parameters - method options and parameters
     */
    ServicePublic.prototype.GetDocumentMenuList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Document/GetDocumentMenuList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取文档菜单列表
     * @method
     * @name ServicePublic#GetDocumentList
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.documentMenuId - 文档id
     * @param {string} parameters.keyWords - 关键词搜索
     * @param {integer} parameters.pageIndex - 当前页
     * @param {integer} parameters.pageSize - 每页条数
     */
    ServicePublic.prototype.GetDocumentList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Document/GetDocumentList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['documentMenuId'] !== undefined) {
            queryParameters['documentMenuId'] = parameters['documentMenuId'];
        }

        if (parameters['keyWords'] !== undefined) {
            queryParameters['keyWords'] = parameters['keyWords'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取文档详情
     * @method
     * @name ServicePublic#GetDocumentDetailById
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.documentMenuId - 文档id
     */
    ServicePublic.prototype.GetDocumentDetailById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Document/GetDocumentDetailById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['documentMenuId'] !== undefined) {
            queryParameters['documentMenuId'] = parameters['documentMenuId'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 评价文档
     * @method
     * @name ServicePublic#EditDocument
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.documentMenuId - 文档id
     * @param {integer} parameters.isSolve - 是否解决：0=未解决，1=已解决
     */
    ServicePublic.prototype.EditDocument = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Document/EditDocument';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['documentMenuId'] !== undefined) {
            queryParameters['documentMenuId'] = parameters['documentMenuId'];
        }

        if (parameters['isSolve'] !== undefined) {
            queryParameters['isSolve'] = parameters['isSolve'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 搜索物流信息
     * @method
     * @name ServicePublic#GetExpressList
     * @param {object} parameters - method options and parameters
     * @param {string} parameters.keyWords - 搜索关键词
     * @param {boolean} parameters.isDeleted - 是否删除
     * @param {integer} parameters.pageIndex - 当前页
     * @param {integer} parameters.pageSize - 每页显示条数
     */
    ServicePublic.prototype.GetExpressList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Express/GetExpressList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        if (parameters['keyWords'] !== undefined) {
            queryParameters['keyWords'] = parameters['keyWords'];
        }

        if (parameters['isDeleted'] !== undefined) {
            queryParameters['isDeleted'] = parameters['isDeleted'];
        }

        /** set default value **/
        queryParameters['pageIndex'] = 1;

        if (parameters['pageIndex'] !== undefined) {
            queryParameters['pageIndex'] = parameters['pageIndex'];
        }

        /** set default value **/
        queryParameters['pageSize'] = 15;

        if (parameters['pageSize'] !== undefined) {
            queryParameters['pageSize'] = parameters['pageSize'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 根据物流id,获取物流信息
     * @method
     * @name ServicePublic#GetExpressById
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.id - 物流id
     */
    ServicePublic.prototype.GetExpressById = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Express/GetExpressById';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['id'] !== undefined) {
            queryParameters['id'] = parameters['id'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 注册行为校验
     * @method
     * @name ServicePublic#GeetestRegister
     * @param {object} parameters - method options and parameters
     */
    ServicePublic.prototype.GeetestRegister = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/GeeTest/GeetestRegister';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取支付路由
     * @method
     * @name ServicePublic#GetPayRoutes
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.applyCode - 应用code:2000=PC商城
     */
    ServicePublic.prototype.GetPayRoutes = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/PayRoute/GetPayRoutes';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        if (parameters['applyCode'] !== undefined) {
            queryParameters['applyCode'] = parameters['applyCode'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 是否登录
     * @method
     * @name ServicePublic#IsLogin
     * @param {object} parameters - method options and parameters
     */
    ServicePublic.prototype.IsLogin = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Public/IsLogin';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 发送短信验证码
     * @method
     * @name ServicePublic#SendSms
     * @param {object} parameters - method options and parameters
     * @param {} parameters.smsInput - 
     */
    ServicePublic.prototype.SendSms = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Sms/SendSms';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['smsInput'] !== undefined) {
            body = parameters['smsInput'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 校验短信验证码
     * @method
     * @name ServicePublic#CheckSmsCode
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServicePublic.prototype.CheckSmsCode = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/Sms/CheckSmsCode';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取经营企业列表
     * @method
     * @name ServicePublic#GetBusinessLicenseList
     * @param {object} parameters - method options and parameters
     */
    ServicePublic.prototype.GetBusinessLicenseList = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/StoreBusinessInfo/GetBusinessLicenseList';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = ['text/plain, application/json, text/json'];
        headers['Content-Type'] = [''];

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 添加热搜词
     * @method
     * @name ServicePublic#AddUserSearchKey
     * @param {object} parameters - method options and parameters
     * @param {} parameters.input - 
     */
    ServicePublic.prototype.AddUserSearchKey = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/UserSearchKey/AddUserSearchKey';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = ['application/json-patch+json,application/json,text/json,application/*+json'];

        if (parameters['input'] !== undefined) {
            body = parameters['input'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('POST', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };
    /**
     * 获取热搜词
     * @method
     * @name ServicePublic#GetUserSearchKeys
     * @param {object} parameters - method options and parameters
     * @param {integer} parameters.topNum - 
     */
    ServicePublic.prototype.GetUserSearchKeys = function(parameters) {
        if (parameters === undefined) {
            parameters = {};
        }
        let deferred = Q.defer();
        let domain = this.domain,
            path = '/api/UserSearchKey/GetUserSearchKeys';
        let body = {},
            queryParameters = {},
            headers = {},
            form = {};

        headers = this.setAuthHeaders(headers);
        headers['Accept'] = [''];
        headers['Content-Type'] = [''];

        /** set default value **/
        queryParameters['topNum'] = 5;

        if (parameters['topNum'] !== undefined) {
            queryParameters['topNum'] = parameters['topNum'];
        }

        queryParameters = mergeQueryParams(parameters, queryParameters);

        superagentRequest('GET', domain + path, body, headers, queryParameters, form, deferred.reject, deferred.resolve);

        return deferred.promise;
    };

    return ServicePublic;
})();

export default ServicePublic;