import { configure } from 'mobx';

/* 开启严格模式 */
configure({ enforceActions: 'observed' });

import { UserState } from './UserState';
import { FrameState } from './FrameState';

export const userState = new UserState();
export const frameState = new FrameState();

export default {
  userState,
  frameState
};

export * from 'mobx';
