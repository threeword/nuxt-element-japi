import { observable, action } from 'mobx';
import { persistence, getStore } from '@/common/index';
const debug = require('debug')('worker:frameState');

const cacheKey = 'FRAME_STATE';
const whiteList = ['menuList', 'menuCollapse', 'appName'];


export class FrameState {
  @observable state = {
    /** 应用名称 */
    appName: '海鸥商城',
    /* 菜单是否收起 */
    menuCollapse: false,
    /* 面包屑数据 */
    breadcrumbData: [],
    /* 面包屑状态 */
    showBreadcrumb: true,
    /* 头部栏loading */
    headerLoading: false,
    /** 父级menu索引 */
    parentMenuIndex: 0,
    /** 父级menu索引 */
    childMenuIndex: '0_0',
    /** 菜单列表 */
    menuList: [],
    ...getStore(cacheKey)
  };

  /** 修改用户状态 */
  @action.bound setFrameState(data) {
    debug('frame状态更新', data);
    const preState = this.state;
    this.state = {
      ...preState,
      ...data
    };

    persistence(cacheKey, this.state, whiteList);
  }

  /** 设置父级选中项 */
  @action.bound setParentMenuIndex(parentMenuIndex) {
    debug('父级菜单index设置', parentMenuIndex);
    const preState = this.state;
    this.state = {
      ...preState,
      parentMenuIndex,
    };
  }

  /** 设置父级选中项 */
  @action.bound setChildMenuIndex(childMenuIndex) {
    debug('二三级菜单index设置', childMenuIndex);
    const preState = this.state;
    this.state = {
      ...preState,
      childMenuIndex,
    };
  }

  /** 设置菜单列表 */
  @action.bound setMenu(data) {
    debug('设置菜单列表', data);
    const preState = this.state;
    this.state = {
      ...preState,
      menuList: data,
    };
    persistence(cacheKey, this.state, whiteList);
  }

  /** 展示或隐藏header loading */
  @action.bound showHeaderLoading(data) {
    debug('展示或隐藏header loading', data);
    const preState = this.state;
    this.state = {
      ...preState,
      headerLoading: data,
    };
  }
}
