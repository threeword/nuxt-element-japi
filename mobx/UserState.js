import { observable, action } from 'mobx';
import { persistence, getStore } from '@/common/index';
const debug = require('debug')('worker:userState');

const cacheKey = 'USER_STATE';
const whiteList = ['isVisitor', 'avatar', 'nickName', 'phone', 'phoneAreaCode'];


export class UserState {
  @observable state = {
    isVisitor: true,
    /** 头像 */
    avatar: 'avatar',
    /** 等级图标 */
    levelIcon: 'levelIcon',
    /** 昵称 */
    nickName: 'nickName',
    /** 手机号 */
    phone: 'phone',
    /** 手机区号 */
    phoneAreaCode: 'phoneAreaCode',
    ...getStore(cacheKey)
  };

  /** 修改用户状态 */
  @action.bound setUserState(data) {
    debug('用户状态更新', data);
    const preState = this.state;
    this.state = {
      ...preState,
      ...data
    };
    
    persistence(cacheKey, this.state, whiteList);

  }

  // @action.bound incrementAsync() {
  //   setTimeout(() => {
  //     this.counter++
  //   }, 1000)
  // }
}
